<?php
// $Id$

/**
 * Implementation of hook_install().
 */
function lenses_install() {
  $success = FALSE;
  switch ($GLOBALS['db_type']) {
    case 'mysql':
    case 'mysqli':
      db_query("CREATE TABLE {lenses} (
        lid int unsigned NOT NULL auto_increment,
        lens varchar(64) NOT NULL,
        name varchar(255) NOT NULL,
        description mediumtext NOT NULL,
        weight int NOT NULL DEFAULT 0,
        uid int unsigned NOT NULL DEFAULT 0,
        privacy tinyint NOT NULL DEFAULT 0,
        show_type tinyint NOT NULL DEFAULT 0,
        view_menu_type tinyint NOT NULL DEFAULT 0,
        edit_menu_type tinyint NOT NULL DEFAULT 0,
        create_menu_type tinyint NOT NULL DEFAULT 0,
        PRIMARY KEY (lid),
        UNIQUE KEY (lens),
        KEY (uid),
        KEY (view_menu_type),
        KEY (edit_menu_type),
        KEY (create_menu_type)
      ) /*!40100 DEFAULT CHARACTER SET UTF8 */ ");

      db_query("CREATE TABLE {lenses_access} (
        lid int unsigned NOT NULL,
        rid int unsigned NOT NULL,
        PRIMARY KEY (lid, rid)
      ) /*!40100 DEFAULT CHARACTER SET UTF8 */ ");

      db_query("CREATE TABLE {lenses_fields} (
        lid int unsigned NOT NULL,
        field_module varchar(64) NOT NULL,
        field_type varchar(64) NOT NULL,
        field varchar(64) NOT NULL,
        PRIMARY KEY (lid, field_module, field_type, field)
      ) /*!40100 DEFAULT CHARACTER SET UTF8 */ ");

      db_query("CREATE TABLE {lenses_node_types} (
        lid int unsigned NOT NULL,
        node_type varchar(32) NOT NULL,
        PRIMARY KEY (lid, node_type)
      ) /*!40100 DEFAULT CHARACTER SET UTF8 */ ");

      $success = TRUE;
      break;

    case 'pgsql':
      db_query("CREATE TABLE {lenses} (
        lid serial CHECK (lid >= 0),
        lens varchar(64) NOT NULL,
        name varchar(255) NOT NULL,
        description text NOT NULL,
        weight int NOT NULL DEFAULT 0,
        uid int_unsigned NOT NULL DEFAULT 0,
        privacy smallint NOT NULL DEFAULT 0,
        show_type smallint NOT NULL DEFAULT 0,
        view_menu_type smallint NOT NULL DEFAULT 0,
        edit_menu_type smallint NOT NULL DEFAULT 0,
        create_menu_type smallint NOT NULL DEFAULT 0,
        PRIMARY KEY (lid),
        UNIQUE (lens)
      )");
      db_query("CREATE INDEX {lenses}_uid_idx ON {lenses} (uid)");
      db_query("CREATE INDEX {lenses}_view_menu_type_idx ON {lenses} (view_menu_type)");
      db_query("CREATE INDEX {lenses}_edit_menu_type_idx ON {lenses} (edit_menu_type)");
      db_query("CREATE INDEX {lenses}_create_menu_type_idx ON {lenses} (create_menu_type)");

      db_query("CREATE TABLE {lenses_access} (
        lid int_unsigned NOT NULL,
        rid int_unsigned NOT NULL,
        PRIMARY KEY (lid, rid)
      )");

      db_query("CREATE TABLE {lenses_fields} (
        lid int_unsigned NOT NULL,
        field_module varchar(64) NOT NULL,
        field_type varchar(64) NOT NULL,
        field varchar(64) NOT NULL,
        PRIMARY KEY (lid, field_module, field_type, field)
      )");

      db_query("CREATE TABLE {lenses_node_types} (
        lid int_unsigned NOT NULL,
        node_type varchar(32) NOT NULL,
        PRIMARY KEY (lid, node_type)
      )");

      $success = TRUE;
      break;

    default:
      drupal_set_message(t('The Lenses module could not be installed because your database is unsupported.'), 'error');
  }

  // We set a high weight to make sure that lenses are applied after other
  // modules may have added fields to a node or a form.
  db_query("UPDATE {system} SET weight = 15 WHERE name = 'lenses'");

  if ($success) {
    drupal_set_message(t('The Lenses module has been successfully installed.'));
  }
  else {
    drupal_set_message(t('The Lenses module could not be installed correctly.'), 'error');
  }
}


/**
 * Implementation of hook_uninstall().
 */
function lenses_uninstall() {
  if (db_table_exists('lenses')) {
    db_query("DROP TABLE {lenses}");
  }
  if (db_table_exists('lenses_access')) {
    db_query("DROP TABLE {lenses_access}");
  }
  if (db_table_exists('lenses_fields')) {
    db_query("DROP TABLE {lenses_fields}");
  }
  if (db_table_exists('lenses_node_types')) {
    db_query("DROP TABLE {lenses_node_types}");
  }
  variable_del('lenses_default_lenses');
  variable_del('lenses_system_default_lenses');
}
